INSERT INTO course_trainee_management_app.course
VALUES (103,'Compiler Design'),(104,'Database Management');

INSERT INTO course_trainee_management_app.user
VALUES (1,'Soumik','Sarker', 'soumik@admin.com', MD5('p1234'), 'admin'),
       (2,'Amla','Khan', 'amla@gmail.com', MD5('amla'), 'trainee');

INSERT INTO course_enrollment_management_app.trainee
VALUES (2);

INSERT INTO course_trainee_management_app.enrollment
VALUES (1,103,2);