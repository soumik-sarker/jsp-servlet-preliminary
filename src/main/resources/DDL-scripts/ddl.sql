-- CREATE TABLE course_trainee_management_app.course (
--     id int NOT NULL AUTO_INCREMENT,
--     name varchar(100) NOT NULL,
--     PRIMARY KEY (id),
--     UNIQUE KEY name (name)
-- );

CREATE TABLE course_trainee_management_app.user (
    id int NOT NULL AUTO_INCREMENT,
    first_name varchar(100) NOT NULL,
    last_name varchar(100) NOT NULL,
    gmail varchar(100) NOT NULL,
    password varchar(255) NOT NULL,
    role varchar(100) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY gmail (gmail)
);
-- Showing error
-- CREATE TABLE course_trainee_management_app.trainee (
--     id int NOT NULL,
--     FOREIGN KEY (id) REFERENCES course_trainee_management_app.user (id),
-- );

-- CREATE TABLE course_trainee_management_app.trainee  (
--    id int NOT NULL ,
--    PRIMARY KEY ( id )
-- );
--
-- -- CREATE TABLE trainee (
-- --     id int NOT NULL AUTO_INCREMENT,
-- --     first_name varchar(100) NOT NULL,
-- --     last_name varchar(100) NOT NULL,
-- --     PRIMARY KEY (id)
-- -- );
--
-- CREATE TABLE course_trainee_management_app.enrollment (
--     id int NOT NULL AUTO_INCREMENT,
--     course_id int NOT NULL,
--     trainee_id int NOT NULL,
--     PRIMARY KEY (id),
--     KEY course_id (course_id),
--     KEY trainee_id (trainee_id),
--     FOREIGN KEY (course_id) REFERENCES course_trainee_management_app.course (id) ON DELETE CASCADE,
--     FOREIGN KEY (trainee_id) REFERENCES course_trainee_management_app.trainee (id) ON DELETE CASCADE
-- );
-- MUST use ON DELETE CASCADE - otherwise show error while deleting course or trainee
--
--
-- --Have to alter for using Inheritance.JOINED
-- -- create table Asian_Country (continent varchar2(255 char), population number(10,0) not null, id number(10,0) not null, primary key (id))
-- -- create table Country (id number(10,0) not null, name varchar2(255 char), primary key (id))
-- -- create table European_Country (continent varchar2(255 char), population number(10,0) not null, id number(10,0) not null, primary key (id))
-- -- alter table Asian_Country add constraint FK7kqwyl67s72y7t73lhoh9wtyl foreign key (id) references Country
-- -- alter table European_Country add constraint FK58n74dtysb9tljei1dyhpenj2 foreign key (id) references Country
--
-- create table course_trainee_management_app.trainee (
--                                                        id int NOT NULL ,
--                                                        PRIMARY KEY ( id )
-- );
--
-- create table course_trainee_management_app.user (
--                                                     id int NOT NULL AUTO_INCREMENT,
--                                                     first_name varchar(100) NOT NULL,
--                                                     last_name varchar(100) NOT NULL,
--                                                     gmail varchar(100) NOT NULL,
--                                                     password varchar(255) NOT NULL,
--                                                     role varchar(100) NOT NULL,
--                                                     PRIMARY KEY (id),
--                                                     UNIQUE KEY gmail (gmail)
-- );
--
-- alter table course_trainee_management_app.trainee
--     add constraint FK2nshngrop6dt5amv1egecvdnn
--         foreign key (id)
--             references course_trainee_management_app.user (id);