package net.therap.coursetraineemanagement; /**
 * @author soumik.sarker
 * @since 9/7/21
 */

import net.therap.coursetraineemanagement.domain.Trainee;
import net.therap.coursetraineemanagement.domain.User;
import net.therap.coursetraineemanagement.helper.HashGeneration;
import net.therap.coursetraineemanagement.utils.ConnectionManager;

import javax.persistence.EntityManager;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

public class HelloServlet extends HttpServlet {

    private String message;

    @Override
    public void init() throws ServletException {
        message = "Hello World";
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.sendRedirect("index.jsp");
    }
}
