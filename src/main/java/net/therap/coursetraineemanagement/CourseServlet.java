package net.therap.coursetraineemanagement; /**
 * @author soumik.sarker
 * @since 9/7/21
 */

import net.therap.coursetraineemanagement.domain.Course;
import net.therap.coursetraineemanagement.domain.Trainee;
import net.therap.coursetraineemanagement.service.CourseService;
import net.therap.coursetraineemanagement.service.EnrollmentService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

public class CourseServlet extends HttpServlet {

    //private static final long serialVersionUID = 2L;

    private CourseService courseService;

    private EnrollmentService enrollmentService;

    @Override
    public void init() throws ServletException {
        super.init();
        this.courseService = new CourseService();
        this.enrollmentService = new EnrollmentService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        HttpSession session = request.getSession(false);
//        if (session.getAttribute("authenticated") == null) {
//            response.sendRedirect("/index.jsp");
//            return;
//        }
        String action = request.getParameter("action");

        switch (action) {
            case "addNewCourse":
                addNew(request, response);
                break;

            case "updateCourse":
                update(request, response);
                break;
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        HttpSession session = request.getSession(false);
//
//        if (session.getAttribute("authenticated") == null) {
//            response.sendRedirect("/index.jsp");
//            return;
//        }
        String action = request.getParameter("action");

        if (action == null) {
            action = "courseList";
        }

        switch (action) {

            case "loadUpdateCourseForm":
                showUpdateCourseForm(request, response);
                break;

            case "showCourseDetails":
                find(request, response);
                break;

            case "deleteCourse":
                delete(request, response);
                break;

            case "courseList":
                findAll(request, response);
                break;
        }
    }

    private void addNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String name = request.getParameter("courseName");
        Course course = new Course();
        course.setName(name);

        //courseService = new CourseService();
        courseService.saveOrUpdate(course);
        //response.sendRedirect("courseList");
        response.sendRedirect("/showCourseList");
    }

    private void showUpdateCourseForm(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("courseId"));
        //courseService = new CourseService();
        Course course = courseService.find(id);

        request.setAttribute("course", course);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("updateCourse.jsp");
        requestDispatcher.forward(request, response);
    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("courseId"));
        String name = request.getParameter("courseName");

        //courseService = new CourseService();
        Course course = courseService.find(id);
        course.setName(name);

        courseService.saveOrUpdate(course);
        //response.sendRedirect("courseList");
        response.sendRedirect("/showCourseList");
    }

    private void find(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("courseId"));
        Course course = courseService.find(id);

        List<Trainee> enrolledTraineeList = enrollmentService.findAllEnrolledTraineesByCourse(course);
        for(Trainee trainee: enrolledTraineeList) {
            System.out.println(trainee.getFirstName() + " " + trainee.getLastName());
        }

        request.setAttribute("course", course);
        request.setAttribute("enrolledTraineeList", enrolledTraineeList);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/displayCourseDetails.jsp");
        requestDispatcher.forward(request, response);
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter("courseId"));
        //courseService = new CourseService();
        Course course = courseService.find(id);

        courseService.delete(course);
        //response.sendRedirect("courseList");
        response.sendRedirect("/showCourseList");
    }

    private void findAll(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        //courseService = new CourseService();
        List<Course> courseList = courseService.findAll();

        request.setAttribute("courseList", courseList);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/showCourseList.jsp");
        requestDispatcher.forward(request, response);
    }
}
