package net.therap.coursetraineemanagement;

import net.therap.coursetraineemanagement.domain.User;
import net.therap.coursetraineemanagement.helper.Authentication;
import net.therap.coursetraineemanagement.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author soumik.sarker
 * @since 9/7/21
 */

public class LoginServlet extends HttpServlet {

    //private static final long serialVersionUID = 11212L;

    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String gmail = request.getParameter("gmail");
        String password = request.getParameter("password");

        userService = new UserService();
        User user = userService.findByGmail(gmail);

        if(user != null && Authentication.authCheck(user, password)) {
            HttpSession session = request.getSession();
            session.setAttribute("user", user);

            if("admin".equals(user.getRole())) {
                response.sendRedirect("welcomeAdmin.jsp");
            } else {
                response.sendRedirect("welcomeTrainee.jsp");
            }

        } else {
            response.sendRedirect("login.jsp");
        }
    }
}

