package net.therap.coursetraineemanagement; /**
 * @author soumik.sarker
 * @since 9/7/21
 */

import net.therap.coursetraineemanagement.domain.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UserProfileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        if("admin".equals(user.getRole())) {
            response.sendRedirect("welcomeAdmin.jsp");
        } else {
            response.sendRedirect("welcomeTrainee.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Add password changes and other things
    }
}
