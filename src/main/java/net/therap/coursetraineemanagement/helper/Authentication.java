package net.therap.coursetraineemanagement.helper;

import net.therap.coursetraineemanagement.domain.User;

/**
 * @author soumik.sarker
 * @since 9/7/21
 */
public class Authentication {

    public static Boolean authCheck(User user, String inputPassword) {
        String hashInput = HashGeneration.getMd5(inputPassword);

        if(hashInput.equals(user.getPassword())) {
            return true;
        } else {
            return false;
        }
    }
}
