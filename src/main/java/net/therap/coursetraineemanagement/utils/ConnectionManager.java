package net.therap.coursetraineemanagement.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author soumik.sarker
 * @since 9/7/21
 */
public class ConnectionManager {

    public static EntityManager init() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("database-configuration");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        return entityManager;
    }
}
