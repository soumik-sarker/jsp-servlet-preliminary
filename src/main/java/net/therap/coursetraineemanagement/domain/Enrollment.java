package net.therap.coursetraineemanagement.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author soumik.sarker
 * @since 9/8/21
 */
@Entity
@Table(name = "enrollment")
@NamedQueries({
        @NamedQuery(name = "Enrollment.find",
                query = "SELECT e FROM Enrollment e WHERE e.course = :course AND e.trainee = :trainee"),

        @NamedQuery(name = "Enrollment.findAll", query = "SELECT e FROM Enrollment e")
})
public class Enrollment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private int id;

    @ManyToOne
    private Course course;

    @ManyToOne
    private Trainee trainee;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Trainee getTrainee() {
        return trainee;
    }

    public void setTrainee(Trainee trainee) {
        this.trainee = trainee;
    }

    public boolean isNew() {
        return this.id == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Enrollment enrollment = (Enrollment) o;
        return id == enrollment.id;
    }
}
