package net.therap.coursetraineemanagement.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/8/21
 */
@Entity
@Table(name = "trainee")
@NamedQueries({
        //May not work - should add Join - NO NEED-WORKING FINE
        @NamedQuery(name = "Trainee.findByName",
                query = "SELECT t FROM Trainee t WHERE t.firstName = :firstName AND t.lastName = :lastName"),

        //Should add join - NO NEED-WORKING FINE
        @NamedQuery(name = "Trainee.findAll", query = "SELECT t FROM Trainee t")
})
public class Trainee extends User {

    @OneToMany(mappedBy = "trainee")
    List<Enrollment> enrollmentList = new ArrayList<>();

    public List<Enrollment> getEnrollmentList() {
        return enrollmentList;
    }

    public void setEnrollmentList(List<Enrollment> enrollmentList) {
        this.enrollmentList = enrollmentList;
    }
}
