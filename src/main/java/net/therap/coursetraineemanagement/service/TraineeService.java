package net.therap.coursetraineemanagement.service;

import net.therap.coursetraineemanagement.dao.TraineeDao;
import net.therap.coursetraineemanagement.domain.Trainee;

import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/8/21
 */
public class TraineeService {

    private TraineeDao traineeDao;

    public TraineeService() {
        this.traineeDao = new TraineeDao();
    }

    public void saveOrUpdate(Trainee trainee) {
        if(trainee.isNew()) {
            traineeDao.save(trainee);
        } else {
            traineeDao.update(trainee);
        }
    }

    public void delete(Trainee trainee) {
        traineeDao.delete(trainee);
    }

    public Trainee find(int id) {
        Trainee foundTrainee = traineeDao.find(id);
        return foundTrainee;
    }

    public Trainee findByName(String firstName, String lastName) {
        Trainee foundTrainee = traineeDao.findByName(firstName, lastName);
        return foundTrainee;
    }

    public List<Trainee> findAll() {
        List<Trainee> traineeList = traineeDao.findAll();
        return traineeList;
    }
}
