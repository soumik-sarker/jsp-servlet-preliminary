package net.therap.coursetraineemanagement.service;

import net.therap.coursetraineemanagement.dao.CourseDao;
import net.therap.coursetraineemanagement.domain.Course;

import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/7/21
 */
public class CourseService {

    private CourseDao courseDao;

    public CourseService() {
        this.courseDao = new CourseDao();
    }

    public void saveOrUpdate(Course course) {
        if(course.isNew()) {
            courseDao.save(course);
        } else {
            courseDao.update(course);
        }
    }

    public void delete(Course course) {
        courseDao.delete(course);
    }

    public Course find(int id) {
        Course foundCourse = courseDao.find(id);
        return foundCourse;
    }

    public Course findByName(String name) {
        Course foundCourse = courseDao.findByName(name);
        return foundCourse;
    }

    public List<Course> findAll() {
        List<Course> courseList = courseDao.findAll();
        return courseList;
    }
}
