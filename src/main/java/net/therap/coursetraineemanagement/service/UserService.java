package net.therap.coursetraineemanagement.service;

import net.therap.coursetraineemanagement.dao.UserDao;
import net.therap.coursetraineemanagement.domain.User;

import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/7/21
 */
public class UserService {

    private UserDao userDao;

    public UserService() {
        this.userDao = new UserDao();
    }

    public void saveOrUpdate(User user) {
        if(user.isNew()) {
            userDao.save(user);
        } else {
            userDao.update(user);
        }
    }

    public User find(int id) {
        User foundUser = userDao.find(id);
        return foundUser;
    }

    public User findByGmail(String gmail) {
        User foundUser = userDao.findByGmail(gmail);
        return foundUser;
    }

    public void delete(User user) {
        userDao.delete(user);
    }

    public List<User> findAll() {
        List<User> userList = userDao.findAll();
        return userList;
    }
}
