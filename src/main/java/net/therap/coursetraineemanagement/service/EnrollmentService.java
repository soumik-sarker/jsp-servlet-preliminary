package net.therap.coursetraineemanagement.service;

import net.therap.coursetraineemanagement.dao.EnrollmentDao;
import net.therap.coursetraineemanagement.domain.Course;
import net.therap.coursetraineemanagement.domain.Enrollment;
import net.therap.coursetraineemanagement.domain.Trainee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author soumik.sarker
 * @since 9/8/21
 */
public class EnrollmentService {
    private EnrollmentDao enrollmentDao;

    public EnrollmentService() {
        this.enrollmentDao = new EnrollmentDao();
    }

    public void assignTrainee(Enrollment enrollment) {
        Enrollment foundEnrollment = enrollmentDao.findEnrollmentByCourseAndTrainee(enrollment);

        if(foundEnrollment != null) {
            System.out.println("This trainee is already assigned to this course.");
        } else {
            enrollmentDao.save(enrollment);
        }
    }

    public void dismissTrainee(Enrollment enrollment) {
        Enrollment foundEnrollment = enrollmentDao.findEnrollmentByCourseAndTrainee(enrollment);

        if(foundEnrollment != null) {
            enrollmentDao.delete(foundEnrollment);
        } else {
            System.out.println("No such enrollment found.");
        }
    }

    //Do not use now
    public List<Trainee> findAllEnrolledTraineesByCourse(Course course) {
        List<Enrollment> enrollmentList = enrollmentDao.findEnrollmentByCourseId(course.getId());
        List<Trainee> traineeList = new ArrayList<>();

        for(Enrollment enrollment: enrollmentList){
            traineeList.add(enrollment.getTrainee());
        }

        return traineeList;
    }

    //Do not use now
    public List<Course> findAllEnrolledCoursesByTrainee(Trainee trainee) {
        List<Enrollment> enrollmentList = enrollmentDao.findEnrollmentByTraineeId(trainee.getId());
        List<Course> courseList = new ArrayList<>();

        for(Enrollment enrollment: enrollmentList){
            courseList.add(enrollment.getCourse());
        }

        return courseList;
    }

    public Map<Course, List<Trainee>> findAllTraineeAssignedCourses() {
        Map<Course, List<Trainee>> enrolledCourseTraineeData = new HashMap<>();
        List<Enrollment> enrolledDataList = enrollmentDao.findAllEnrollmentData();

        for (Enrollment enrollment : enrolledDataList) {
            if (!enrolledCourseTraineeData.containsKey(enrollment.getCourse())) {
                enrolledCourseTraineeData.put(enrollment.getCourse(), new ArrayList());
            }
            enrolledCourseTraineeData.get(enrollment.getCourse()).add(enrollment.getTrainee());
        }

        return enrolledCourseTraineeData;
    }
}
