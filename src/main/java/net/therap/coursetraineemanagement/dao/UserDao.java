package net.therap.coursetraineemanagement.dao;

import net.therap.coursetraineemanagement.domain.User;
import net.therap.coursetraineemanagement.utils.ConnectionManager;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/7/21
 */
public class UserDao {

    private EntityManager entityManager;

    public UserDao() {
        this.entityManager = ConnectionManager.init();
    }

    public void save(User user) {
        entityManager.clear();
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    public void update(User user) {
        entityManager.getTransaction().begin();
        entityManager.merge(user);
        entityManager.getTransaction().commit();
    }

    public void delete(User user) {
        entityManager.getTransaction().begin();
        entityManager.remove(user);
        entityManager.getTransaction().commit();
    }

    public User find(int id) {
        User user = entityManager.find(User.class, id);
        return user;
    }

    public User findByGmail(String gmail) {

        User user = entityManager.createNamedQuery("User.findByGmail", User.class)
                .setParameter("gmail", gmail)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

        return user;
    }

    public List<User> findAll() {
        TypedQuery<User> typedQuery = entityManager.createNamedQuery("User.findAll", User.class);
        List<User> userList = typedQuery.getResultList();

        return userList;
    }
}
