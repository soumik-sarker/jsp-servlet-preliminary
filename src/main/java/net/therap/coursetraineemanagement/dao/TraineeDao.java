package net.therap.coursetraineemanagement.dao;

import net.therap.coursetraineemanagement.domain.Trainee;
import net.therap.coursetraineemanagement.utils.ConnectionManager;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/8/21
 */
public class TraineeDao {

    private EntityManager entityManager;

    public TraineeDao() {
        this.entityManager = ConnectionManager.init();
    }

    public void save(Trainee trainee) {
        entityManager.clear();
        entityManager.getTransaction().begin();
        entityManager.persist(trainee);
        entityManager.getTransaction().commit();
    }

    public void update(Trainee trainee) {
        entityManager.getTransaction().begin();
        entityManager.merge(trainee);
        entityManager.getTransaction().commit();
    }

    public void delete(Trainee trainee) {
        entityManager.getTransaction().begin();
        entityManager.remove(trainee);
        entityManager.getTransaction().commit();
    }

    //Confused with this - can it retrieved superclass value also??
    public Trainee find(int id) {
        Trainee trainee = entityManager.find(Trainee.class, id);
        return trainee;
    }

    public Trainee findByName(String firstName, String lastName) {

        Trainee trainee = entityManager.createNamedQuery("Trainee.findByName", Trainee.class)
                .setParameter("firstName", firstName)
                .setParameter("lastName", lastName)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

        return trainee;
    }

    public List<Trainee> findAll() {
        TypedQuery<Trainee> typedQuery = entityManager.createNamedQuery("Trainee.findAll", Trainee.class);
        List<Trainee> traineeList = typedQuery.getResultList();

        return traineeList;
    }
}
