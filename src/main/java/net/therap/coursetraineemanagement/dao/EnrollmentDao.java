package net.therap.coursetraineemanagement.dao;

import net.therap.coursetraineemanagement.domain.Course;
import net.therap.coursetraineemanagement.domain.Enrollment;
import net.therap.coursetraineemanagement.domain.Trainee;
import net.therap.coursetraineemanagement.utils.ConnectionManager;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/8/21
 */
public class EnrollmentDao {

    private EntityManager entityManager;

    public EnrollmentDao() {
        this.entityManager = ConnectionManager.init();
    }

    public void save(Enrollment enrollment) {
        entityManager.clear();
        entityManager.getTransaction().begin();
        entityManager.persist(enrollment);
        entityManager.getTransaction().commit();
    }

    public void delete(Enrollment enrollment) {
        entityManager.getTransaction().begin();
        entityManager.remove(enrollment);
        entityManager.getTransaction().commit();
    }

    public List<Enrollment> findEnrollmentByCourseId(int id) {
        entityManager.clear();
        Course course = entityManager.find(Course.class, id);
        List<Enrollment> enrollmentList = course.getEnrollmentList();

        return enrollmentList;
    }

    public List<Enrollment> findEnrollmentByTraineeId(int id) {
        entityManager.clear();
        Trainee trainee = entityManager.find(Trainee.class, id);
        List<Enrollment> enrollmentList = trainee.getEnrollmentList();

        return enrollmentList;
    }


    public Enrollment findEnrollmentByCourseAndTrainee(Enrollment enrollment) {

        enrollment = entityManager.createNamedQuery("Enrollment.find", Enrollment.class)
                .setParameter("course", enrollment.getCourse())
                .setParameter("trainee", enrollment.getTrainee())
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

        return enrollment;
    }

    public List<Enrollment> findAllEnrollmentData() {
        entityManager.clear();
        TypedQuery<Enrollment> typedQuery = entityManager.createNamedQuery("Enrollment.findAll", Enrollment.class);
        List<Enrollment> enrolledDataList = typedQuery.getResultList();

        return enrolledDataList;
    }
}
