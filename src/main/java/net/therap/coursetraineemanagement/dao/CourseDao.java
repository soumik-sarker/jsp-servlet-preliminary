package net.therap.coursetraineemanagement.dao;

import net.therap.coursetraineemanagement.domain.Course;
import net.therap.coursetraineemanagement.utils.ConnectionManager;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 9/7/21
 */
public class CourseDao {

    private EntityManager entityManager;

    public CourseDao() {
        this.entityManager = ConnectionManager.init();
    }

    public void save(Course course) {
        entityManager.clear(); //otherwise show error while adding two courses one after another
        entityManager.getTransaction().begin();
        entityManager.persist(course);
        entityManager.getTransaction().commit();
    }

    public void update(Course course) {
        //no need to use entityManager.clear() here.
        entityManager.getTransaction().begin();
        entityManager.merge(course);
        entityManager.getTransaction().commit();
    }

    public void delete(Course course) {
        //no need to use entityManager.clear() here.
        entityManager.getTransaction().begin();
        entityManager.remove(course);
        entityManager.getTransaction().commit();
    }

    public Course find(int id) {
        Course course = entityManager.find(Course.class, id);
        return course;
    }

    public Course findByName(String name) {

        Course course = entityManager.createNamedQuery("Course.findByName", Course.class)
                .setParameter("name", name)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

        return course;
    }

    public List<Course> findAll() {
        TypedQuery<Course> typedQuery = entityManager.createNamedQuery("Course.findAll", Course.class);
        List<Course> courseList = typedQuery.getResultList();

        return courseList;
    }
}
