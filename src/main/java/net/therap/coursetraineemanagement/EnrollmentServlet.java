package net.therap.coursetraineemanagement; /**
 * @author soumik.sarker
 * @since 9/8/21
 */

import net.therap.coursetraineemanagement.domain.Course;
import net.therap.coursetraineemanagement.domain.Enrollment;
import net.therap.coursetraineemanagement.domain.Trainee;
import net.therap.coursetraineemanagement.service.CourseService;
import net.therap.coursetraineemanagement.service.EnrollmentService;
import net.therap.coursetraineemanagement.service.TraineeService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet(name = "EnrollmentServlet", value = "/EnrollmentServlet")
public class EnrollmentServlet extends HttpServlet {

    //private static final long serialVersionUID = 2L;

    private EnrollmentService enrollmentService;

    private CourseService courseService;

    private TraineeService traineeService;

    @Override
    public void init() throws ServletException {
        super.init();
        this.enrollmentService = new EnrollmentService();
        this.courseService = new CourseService();
        this.traineeService = new TraineeService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        HttpSession session = request.getSession(false);
//        if (session.getAttribute("authenticated") == null) {
//            response.sendRedirect("/index.jsp");
//            return;
//        }
        String action = request.getParameter("action");

        switch (action) {
            case "addNewEnrollment":
                addNew(request, response);
                break;
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        HttpSession session = request.getSession(false);
//
//        if (session.getAttribute("authenticated") == null) {
//            response.sendRedirect("/index.jsp");
//            return;
//        }
        String action = request.getParameter("action");

        if (action == null) {
            action = "enrollmentList";
        }

        switch (action) {

//            case "showEnrollmentDetails":
//                find(request, response);
//                break;

            case "loadAddEnrollmentForm":
                showAddEnrollmentForm(request, response);
                break;

            case "deleteEnrollment":
                delete(request, response);
                break;

            case "enrollmentList":
                findAll(request, response);
                break;
        }
    }

    private void addNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int courseId = Integer.parseInt(request.getParameter("courseId"));
        int traineeId = Integer.parseInt(request.getParameter("traineeId"));

        Course course = courseService.find(courseId);

        //Confused - can it retrive all values of superclass
        Trainee trainee = traineeService.find(traineeId);

        Enrollment enrollment = new Enrollment();

        enrollment.setCourse(course);
        enrollment.setTrainee(trainee);

        enrollmentService.assignTrainee(enrollment);
        response.sendRedirect("/showEnrollmentList");
    }

    private void find(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

    }

    private void showAddEnrollmentForm(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<Course> courseList = courseService.findAll();
        List<Trainee> traineeList = traineeService.findAll();

        request.setAttribute("courseList", courseList);
        request.setAttribute("traineeList", traineeList);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("addNewEnrollment.jsp");
        requestDispatcher.forward(request, response);
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int courseId = Integer.parseInt(request.getParameter("courseId"));
        int traineeId = Integer.parseInt(request.getParameter("traineeId"));

        Course course = courseService.find(courseId);

        //Confused - can it retrive all values of superclass
        Trainee trainee = traineeService.find(traineeId);

        Enrollment enrollment = new Enrollment();

        enrollment.setCourse(course);
        enrollment.setTrainee(trainee);

        enrollmentService.dismissTrainee(enrollment);

        response.sendRedirect("/showEnrollmentList");
    }

    private void findAll(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Map<Course, List<Trainee>> enrolledCourseTraineeData = enrollmentService.findAllTraineeAssignedCourses();

        request.setAttribute("enrolledCourseTraineeData", enrolledCourseTraineeData);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/showEnrollmentList.jsp");
        requestDispatcher.forward(request, response);
    }
}
