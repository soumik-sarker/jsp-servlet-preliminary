package net.therap.coursetraineemanagement; /**
 * @author soumik.sarker
 * @since 9/7/21
 */

import net.therap.coursetraineemanagement.domain.Course;
import net.therap.coursetraineemanagement.domain.Trainee;
import net.therap.coursetraineemanagement.domain.User;
import net.therap.coursetraineemanagement.helper.HashGeneration;
import net.therap.coursetraineemanagement.service.EnrollmentService;
import net.therap.coursetraineemanagement.service.TraineeService;
import net.therap.coursetraineemanagement.service.UserService;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.List;

public class UserServlet extends HttpServlet {

    private UserService userService;

    private TraineeService traineeService;

    private EnrollmentService enrollmentService;

    @Override
    public void init() throws ServletException {
        super.init();
        this.userService = new UserService();
        this.traineeService = new TraineeService();
        this.enrollmentService = new EnrollmentService();
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        HttpSession session = request.getSession(false);
//        if (session.getAttribute("authenticated") == null) {
//            response.sendRedirect("/index.jsp");
//            return;
//        }
        String action = request.getParameter("action");

        switch (action) {
            case "addNewUser":
                addNew(request, response);
                break;

            case "updateUser":
                update(request, response);
                break;
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//        HttpSession session = request.getSession(false);
//
//        if (session.getAttribute("authenticated") == null) {
//            response.sendRedirect("/index.jsp");
//            return;
//        }
        String action = request.getParameter("action");

        if (action == null) {
            action = "userList";
        }

        switch (action) {
            case "loadUpdateUserForm":
                showUpdateUserForm(request, response);
                break;

            case "showUserDetails":
                find(request, response);
                break;

            case "deleteUser":
                delete(request, response);
                break;

            case "userList":
                findAll(request, response);
                break;
        }
    }

    private void addNew(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String gmail = request.getParameter("gmail");

        String role = request.getParameter("role");
        String password = request.getParameter("password");

        String hashedPassword = HashGeneration.getMd5(password);

//        User user = new User();
//        user.setFirstName(firstName);
//        user.setLastName(lastName);
//        user.setGmail(gmail);
//
//        user.setRole(role);
//        user.setPassword(hashedPassword);
//
//        userService.saveOrUpdate(user);

        //pass conditioned service
        if("trainee".equals(role)) {
            Trainee trainee = new Trainee();
            trainee.setFirstName(firstName);
            trainee.setLastName(lastName);
            trainee.setGmail(gmail);

            trainee.setRole(role);
            trainee.setPassword(hashedPassword);

            traineeService.saveOrUpdate(trainee);
        } else { //replace all below these with AdminService and Admin
            User user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setGmail(gmail);

            user.setRole(role);
            user.setPassword(hashedPassword);

            userService.saveOrUpdate(user);
        }

        response.sendRedirect("/showUserList");
    }

    private void showUpdateUserForm(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("userId"));
        User user = userService.find(id);

        request.setAttribute("user", user);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("updateUser.jsp");
        requestDispatcher.forward(request, response);
    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("userId"));
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String gmail = request.getParameter("gmail");

        User user = userService.find(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setGmail(gmail);

        userService.saveOrUpdate(user);
        response.sendRedirect("/showUserList");
    }

    private void find(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        int id = Integer.parseInt(request.getParameter("userId"));
        User user = userService.find(id);

        if("trainee".equals(user.getRole())) {
            Trainee trainee = (Trainee) user;
            List<Course> enrolledCourseList = enrollmentService.findAllEnrolledCoursesByTrainee(trainee);

            for(Course course: enrolledCourseList) {
                System.out.println(course.getName());
            }

            request.setAttribute("enrolledCourseList", enrolledCourseList);
        }

        request.setAttribute("user", user);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/displayUserDetails.jsp");
        requestDispatcher.forward(request, response);
    }

    private void delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter("userId"));
        User user = userService.find(id);

        userService.delete(user);
        response.sendRedirect("/showUserList");
    }

    private void findAll(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        List<User> userList = userService.findAll();

        request.setAttribute("userList", userList);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/showUserList.jsp");
        requestDispatcher.forward(request, response);
    }
}
