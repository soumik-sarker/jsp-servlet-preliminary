<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/7/21
  Time: 12:11 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Add New Course</title>
</head>
<body>
    <jsp:include page="components/navbar.jsp"></jsp:include>
    <br>
<%--    ${pageContext.request.contextPath}/showCourseList/addNewCourse--%>
    <form action="/showCourseList" method="post">
        <input type="hidden" name="action" value="addNewCourse">
        <label for="courseName">Course:</label>
        <input type="text" id="courseName" name="courseName">
        <br><br>
        <input type="submit" value="Save" class="button">
    </form>
</body>
</html>
