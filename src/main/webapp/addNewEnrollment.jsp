<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/8/21
  Time: 5:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Add New Enrollment</title>
</head>
<body>
    <jsp:include page="components/navbar.jsp"></jsp:include>
    <br>
    <form action="/showEnrollmentList" method="post">
        <input type="hidden" name="action" value="addNewEnrollment">

        <label for="course">Choose a course:</label>
        <select name="courseId" id="course">
            <c:forEach var="course" items="${courseList}">
                <option value="${course.getId()}">${course.getName()}</option>
            </c:forEach>
        </select>

        <br><br>

        <label for="trainee">Choose a trainee:</label>
        <select name="traineeId" id="trainee">
            <c:forEach var="trainee" items="${traineeList}">
                <option value="${trainee.getId()}">${trainee.getFirstName()} ${trainee.getLastName()}</option>
            </c:forEach>
        </select>

        <br><br>
        <input type="submit" value="Add New Enrollment" class="button">
    </form>
</body>
</html>
