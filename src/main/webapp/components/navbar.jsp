<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/6/21
  Time: 1:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="styles/styles.css">
    <title>Navbar</title>
</head>
<body>

    <ul>
        <li><a href="/userProfile">Profile</a></li>
        <li><a href="/showUserList">Users</a></li>
        <li><a href="/showCourseList">Courses</a></li>
        <li><a href="/showEnrollmentList">Enrollment</a></li>
        <li style="float:right"><a href="/logout">Log out</a></li>
    </ul>

</body>
</html>
