<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/7/21
  Time: 11:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>All Users</title>
</head>
<body>
<jsp:include page="components/navbar.jsp"></jsp:include>

<div class="row">
    <div class="container">
        <div class="container text-left">
            <input type="button" value="Add new user"
                   onclick="window.location.href='addNewUser.jsp'; return false;"
                   class="button">
        </div>

        <br>

        <table id="table">
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Gmail</th>
                <th>Role</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="user" items="${userList}">
                <c:url var="showDetailsLink" value="/showUserList">
                    <c:param name="action" value="showUserDetails"/>
                    <c:param name="userId" value="${user.getId()}"/>
                </c:url>
                <c:url var="updateLink" value="/showUserList">
                    <c:param name="action" value="loadUpdateUserForm"/>
                    <c:param name="userId" value="${user.getId()}"/>
                </c:url>
                <c:url var="deleteLink" value="/showUserList">
                    <c:param name="action" value="deleteUser"/>
                    <c:param name="userId" value="${user.getId()}"/>
                </c:url>

                <tr>
                    <td>${user.getFirstName()}</td>
                    <td>${user.getLastName()}</td>
                    <td>${user.getGmail()}</td>
                    <td>${user.getRole()}</td>
                    <td>
                        <a href="${showDetailsLink}">Show</a>
                        <a href="${updateLink}">Edit</a>
                        <a href="${deleteLink}">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </table>

    </div>
</div>
</body>
</html>
