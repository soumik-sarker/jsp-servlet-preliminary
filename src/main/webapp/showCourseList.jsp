<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/6/21
  Time: 2:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>All Courses</title>
</head>
<body>
    <jsp:include page="components/navbar.jsp"></jsp:include>

    <div class="row">
<%--        ${pageContext.request.contextPath}/showCourseList/newCourseForm--%>
        <div class="container">
            <div class="container text-left">
<%--                <a href="${pageContext.request.contextPath}/newCourseForm" class="button">Add New Course</a>--%>
<%--                <a href="${pageContext.request.contextPath}/list" class="button">List All Courses</a>--%>
                    <input type="button" value="Add new Course"
                           onclick="window.location.href='addNewCourse.jsp'; return false;"
                           class="button">
            </div>
<%--            <form action="<c:url value="/showCourseList/newCourseForm"/>" method="post">--%>
<%--                <input type="submit" value="Add new Course">--%>
<%--            </form>--%>

            <br>

            <table id="table">
                <tr>
                    <th>Course Name</th>
                    <th>Actions</th>
                </tr>
                <c:forEach var="course" items="${courseList}">
                    <c:url var="showDetailsLink" value="/showCourseList">
                        <c:param name="action" value="showCourseDetails"/>
                        <c:param name="courseId" value="${course.getId()}"/>
                    </c:url>
                    <c:url var="updateLink" value="/showCourseList">
                        <c:param name="action" value="loadUpdateCourseForm"/>
                        <c:param name="courseId" value="${course.getId()}"/>
                    </c:url>
                    <c:url var="deleteLink" value="/showCourseList">
                        <c:param name="action" value="deleteCourse"/>
                        <c:param name="courseId" value="${course.getId()}"/>
                    </c:url>
<%--                    <c:url var="enrollmentLink" value="/showCourseList">--%>
<%--                        <c:param name="command" value="ENROLLED_TRAINEES"/>--%>
<%--                        <c:param name="courseId" value="${course.id}"/>--%>
<%--                    </c:url>--%>
                    <tr>
                        <td>${course.getName()}</td>
                        <td>
                            <a href="${showDetailsLink}">Show</a>
                            <a href="${updateLink}">Edit</a>
                            <a href="${deleteLink}">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>

        </div>
    </div>
</body>
</html>
