<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/7/21
  Time: 12:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Course Details</title>
</head>
<body>
    <jsp:include page="components/navbar.jsp"></jsp:include>


    Course Id: ${course.getId()} <br>
    Course Name: ${course.getName()} <br><br>

    <h2>Enrolled Trainees</h2>

    <table id="table">
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
        </tr>
<%--        <c:if test="${fn:length(enrolledTraineeList) == 0}">}">--%>
<%--            <br>--%>
<%--            <h5> No enrolled courses yet </h5>--%>
<%--        </c:if>--%>
<%--        <c:if test="${fn:length(enrolledTraineeList) > 0}">}">--%>
            <c:forEach var="trainee" items="${enrolledTraineeList}">
                <tr>
                    <td>${trainee.getFirstName()}</td>
                    <td>${trainee.getLastName()}</td>
                </tr>
            </c:forEach>
<%--        </c:if>--%>
    </table>
</body>
</html>