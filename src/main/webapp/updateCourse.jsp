<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/7/21
  Time: 12:12 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>\
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Update Course</title>
</head>
<body>
    <jsp:include page="components/navbar.jsp"></jsp:include>
    <br>
    <form action="/showCourseList" method="post">
        <input type="hidden" name="action" value="updateCourse">
        <input type="hidden" name="courseId" value="${course.getId()}" />
        <label for="courseName">Course:</label>
        <input type="text" id="courseName" value="${course.getName()}" name="courseName">
        <br><br>
        <input type="submit" value="Update" class="button">
    </form>
</body>
</html>
