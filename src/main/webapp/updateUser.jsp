<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/7/21
  Time: 11:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Update User</title>
</head>
<body>
<jsp:include page="components/navbar.jsp"></jsp:include>
<br>
<form action="/showUserList" method="post">
    <input type="hidden" name="action" value="updateUser">
    <input type="hidden" name="userId" value="${user.getId()}" />
    <label for="firstName">First Name:</label>
    <input type="text" id="firstName" value="${user.getFirstName()}" name="firstName"> <br><br>
    <label for="lastName">Last Name:</label>
    <input type="text" id="lastName" value="${user.getLastName()}" name="lastName"> <br><br>
    <label for="gmail">Gmail:</label>
    <input type="text" id="gmail" value="${user.getGmail()}" name="gmail">
    <br><br>
    <input type="submit" value="Update" class="button">
</form>
</body>
</html>
