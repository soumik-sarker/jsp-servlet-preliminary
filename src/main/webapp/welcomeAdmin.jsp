<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/6/21
  Time: 12:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin Welcome Page</title>
</head>
<body>

    <jsp:include page="components/navbar.jsp"></jsp:include>

    Welcome ${user.getFirstName()}

</body>
</html>
