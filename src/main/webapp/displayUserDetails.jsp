<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/7/21
  Time: 11:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <jsp:include page="components/navbar.jsp"></jsp:include>

    User Id: ${user.getId()} <br>
    First Name: ${user.getFirstName()} <br>
    Last Name: ${user.getLastName()} <br><br>

    <c:if test="${user.getRole() == 'trainee'}">

        <h2>Enrolled Courses</h2>

        <table id="table">
            <tr>
                <th>Course Name</th>
            </tr>
<%--            <c:if test="${fn:length(enrolledCourseList) > 0}">}">--%>
                <c:forEach var="course" items="${enrolledCourseList}">
                    <tr>
                        <td>${course.getName()}</td>
                    </tr>
                </c:forEach>
<%--            </c:if>--%>
        </table>
<%--    </c:if>--%>
</body>
</html>
