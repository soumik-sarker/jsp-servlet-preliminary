<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/6/21
  Time: 12:02 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Trainee Welcome Page</title>
</head>
<body>
    <jsp:include page="components/navbar.jsp"></jsp:include>
    Welcome ${user.getGmail()} <br><br>

    <form action="/userProfile" method="post">
        <input type="hidden" name="userId" value="${user.getId()}">
        <label for="password">Enter new password:</label>
        <input type="password" id="password" name="password">
        <br><br>
        <input type="submit" value="Change Password" class="button">
    </form>
</body>
</html>
