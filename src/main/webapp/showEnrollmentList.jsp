<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/8/21
  Time: 12:55 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>All Users</title>
</head>
<body>
<jsp:include page="components/navbar.jsp"></jsp:include>

<div class="row">
    <div class="container">
        <div class="container text-left">
            <form action="/showEnrollmentList" method="get">
                <input type="hidden" name="action" value="loadAddEnrollmentForm">
                <input type="submit" value="Add new Enrollment" class="button">
            </form>
        </div>

        <br>

        <table id="table">
            <tr>
                <th>Course Name</th>
                <th>Trainee First Name</th>
                <th>Trainee Last Name</th>
                <th>Actions</th>
            </tr>
            <c:forEach var="entry" items="${enrolledCourseTraineeData}">
                <c:forEach var="trainee" items="${entry.getValue()}">
<%--                <c:url var="showDetailsLink" value="/showUserList">--%>
<%--                    <c:param name="action" value="showUserDetails"/>--%>
<%--                    <c:param name="userId" value="${user.getId()}"/>--%>
<%--                </c:url>--%>
                    <c:url var="deleteLink" value="/showEnrollmentList">
                        <c:param name="action" value="deleteEnrollment"/>
                        <c:param name="traineeId" value="${trainee.getId()}"/>
                        <%-- Should it be course.getId() or entry.getKey().getId()?? --%>
                        <c:param name="courseId" value="${entry.getKey().getId()}"/>
                    </c:url>

                    <tr>
                        <td>${entry.getKey().getName()}</td>
                        <td>${trainee.getFirstName()}</td>
                        <td>${trainee.getLastName()}</td>
                        <td>
<%--                            <a href="${showDetailsLink}">Show</a>--%>
                            <a href="${deleteLink}">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
            </c:forEach>
        </table>
    </div>
</div>
</body>
</html>
