<%--
  Created by IntelliJ IDEA.
  User: soumik.sarker
  Date: 9/7/21
  Time: 11:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Add New User</title>
</head>
<body>
    <jsp:include page="components/navbar.jsp"></jsp:include>
    <br>
    <form action="/showUserList" method="post">
        <input type="hidden" name="action" value="addNewUser">
        <label for="firstName">First Name:</label>
        <input type="text" id="firstName" name="firstName"> <br><br>
        <label for="lastName">Last Name:</label>
        <input type="text" id="lastName" name="lastName"> <br><br>
        <label for="gmail">Gmail:</label>
        <input type="text" id="gmail" name="gmail"> <br><br>
        <label for="password">Password:</label>
        <input type="password" id="password" name="password"> <br><br>

        <label for="role">Choose a role:</label>
        <select name="role" id="role">
            <option value="admin">admin</option>
            <option value="trainee">trainee</option>
        </select>

        <br><br>
        <input type="submit" value="Save" class="button">
    </form>
</body>
</html>
